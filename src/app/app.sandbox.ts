import { Injectable } from '@angular/core';
import { ApiService } from './services/api.service';
import { Observable } from 'rxjs';
import { SideNavItems } from './models/models';

@Injectable({
  providedIn: 'root'
})
export class AppSandbox {
  constructor(private apiService: ApiService) {}

  getSideNavItems(): Observable<SideNavItems> {
    return this.apiService.getSideNavItems();
  }

  getApiError(): Observable<never> {
    return this.apiService.getApiError();
  }
}
