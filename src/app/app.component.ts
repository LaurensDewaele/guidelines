import { Component, OnInit } from '@angular/core';
import { AppSandbox } from './app.sandbox';
import { SideNavItems } from './models/models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  error: Error;
  sideNavItems: SideNavItems;

  constructor(private sandbox: AppSandbox) {}

  ngOnInit() {
    this.sandbox.getSideNavItems().subscribe(
      (sideNavItems: SideNavItems) => {
        this.sideNavItems = sideNavItems;
      },
      error => {
        this.error = error;
      }
    );
  }
}
