import { flattenDeep, sortBy } from 'lodash';

import {
  GitLabResponseItem,
  SideNavItems,
  SubTopic,
  Topic
} from '../models/models';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export const mapGitLabTopicResponse = (
  response: GitLabResponseItem[]
): Topic[] => {
  const topics: Topic[] = response
    .filter(item => item.type === 'tree' && item.name !== 'assets')
    .map(item => {
      return {
        name: item.name,
        subTopics: []
      };
    });
  return topics;
};

export const mapTopicsToSubTopicRequests = (
  topics: Topic[],
  baseUrl: string,
  httpClient: HttpClient
): Observable<Object>[] => {
  return topics.map(topic => {
    return httpClient.get(encodeURI(`${baseUrl}/tree?path=${topic.name}`));
  });
};

export const mapToSideNavItems = (
  topics: Topic[],
  subTopics: GitLabResponseItem[][]
): SideNavItems => {
  const flattenedSubTopics: GitLabResponseItem[] = flattenDeep(subTopics) as GitLabResponseItem[];
  const formattedSubTopics: SubTopic[] = flattenedSubTopics
    .filter(item => item.type === 'blob')
    .map(item => {
      return {
        topic: item.path.split('/')[0],
        ...item
      };
    });

  const sideNavItems: SideNavItems = topics.map(topic => {
    return {
      name: topic.name,
      subTopics: sortBy(
        formattedSubTopics.filter(subTopic => subTopic.topic === topic.name),
        subTopic => subTopic.name
      )
    };
  });

  return sortBy(sideNavItems, topic => topic.name);
};
