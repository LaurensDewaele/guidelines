import { TestBed } from '@angular/core/testing';

import { AppSandbox } from './app.sandbox';

describe('AppSandbox', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AppSandbox = TestBed.get(AppSandbox);
    expect(service).toBeTruthy();
  });
});
