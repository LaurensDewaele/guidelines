import { Component, Input } from '@angular/core';
import { SideNavItems } from '../../models/models';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html'
})
export class SideNavComponent {
  @Input() sideNavItems: SideNavItems;

  constructor() {}
}
