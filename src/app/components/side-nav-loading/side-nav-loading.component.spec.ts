import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideNavLoadingComponent } from './side-nav-loading.component';

describe('SideNavLoadingComponent', () => {
  let component: SideNavLoadingComponent;
  let fixture: ComponentFixture<SideNavLoadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideNavLoadingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideNavLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
