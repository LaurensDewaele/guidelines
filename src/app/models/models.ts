export type GitLabResponseItemTypes = 'tree' | 'blob';

export interface GitLabResponseItem {
  id: string;
  name: string;
  type: GitLabResponseItemTypes;
  path: string;
  mode: string;
}

export interface SubTopic {
  topic: string;
  id: string;
  name: string;
  type: string;
  path: string;
  mode: string;
}

export interface Topic {
  name: string;
  subTopics: SubTopic[];
}

export type SideNavItems = Topic[];
