import { AfterViewInit, Directive, ElementRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Directive({
  selector: '[appFormatSubTopic]'
})
export class FormatSubTopicDirective implements AfterViewInit {
  liElement: HTMLLIElement;

  constructor(private el: ElementRef, private sanitizer: DomSanitizer) {
    this.liElement = el.nativeElement;
  }

  ngAfterViewInit() {
    const value = this.liElement.innerText;
    const newValue = this.sanitizer.sanitize(1, this.formatSubtopic(value));
    this.liElement.innerText = newValue;
  }

  formatSubtopic(subTopicFileName: string): string {
    return subTopicFileName
      .split('.')[0]
      .split('-')
      .join(' ');
  }
}
