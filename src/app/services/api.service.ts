import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { forkJoin, Observable } from 'rxjs';
import { GitLabResponseItem, SideNavItems, Topic } from '../models/models';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { ErrorHandlerService } from './error-handler.service';
import {
  mapGitLabTopicResponse,
  mapTopicsToSubTopicRequests,
  mapToSideNavItems
} from '../functions/functions';

const GITLAB_API_URL = 'https://gitlab.com/api/v4/projects/10170038/repository';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  URL_TOPICS: string;

  constructor(
    private http: HttpClient,
    private errorHandler: ErrorHandlerService
  ) {
    this.URL_TOPICS = `${GITLAB_API_URL}/tree?per_page=100`;
  }

  getSideNavItems(): Observable<SideNavItems> {
    return this.http.get<GitLabResponseItem[]>(this.URL_TOPICS).pipe(
      catchError((error: HttpErrorResponse) => {
        return this.errorHandler.handleHttpError(error);
      }),
      mergeMap((response: GitLabResponseItem[]) => {
        const topics: Topic[] = mapGitLabTopicResponse(response);
        const subTopicRequests: Observable<
          Object
        >[] = mapTopicsToSubTopicRequests(topics, GITLAB_API_URL, this.http);
        return forkJoin(subTopicRequests).pipe(
          map((subTopics: GitLabResponseItem[][]) =>
            mapToSideNavItems(topics, subTopics)
          )
        );
      })
    );
  }

  getApiError(): Observable<never> {
    return this.http
      .get<never>('https://gitlab.com/api/v4/projects/10170038/repository/INVALID_FCKN_SHIT')
      .pipe(
        catchError((error: HttpErrorResponse) => {
          return this.errorHandler.handleHttpError(error);
        })
      );
  }
}
