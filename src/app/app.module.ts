import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { ContentComponent } from './components/content/content.component';
import { SideNavLoadingComponent } from './components/side-nav-loading/side-nav-loading.component';
import { FormatSubTopicDirective } from './format-sub-topic.directive';

@NgModule({
  declarations: [AppComponent, HeaderComponent, SideNavComponent, ContentComponent, SideNavLoadingComponent, FormatSubTopicDirective],
  imports: [AppRoutingModule, BrowserModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
